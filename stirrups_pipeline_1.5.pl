#! /usr/bin/perl
# Name: stirrups_pipeline_1.0.pl
# Last Update: 1/04/2013 Steven B.
# Function: Takes a collection of reference fasta files and read fasta files and runs a usearch alignment
#   for the files of the same Genus.  Uses the resulting output from uclust to format output and 
#   calculate statistics based on percentage make up of a strain and species for a particular sample id.
#------------------------------------------------------------------------------
#                                     Packages
#------------------------------------------------------------------------------

my $version = "Version 1.0";

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use File::Spec;

#------------------------------------------------------------------------------
#			        File/Input Handling
#------------------------------------------------------------------------------

my $inputLibraryFile; 		               #individual Library file
my $inputReadFile;	       	               #individual Read file
my $threshold = 97;                            #default = 97
my $usearchflag = 1;
my $help;
my $BASEDIR = dirname($0);
my $dostrain = "";
my $usResultsFile;                             #individual usearch output file
my $globloc = "global";
my $rdp_option = '';

my $result = GetOptions ("L=s" => \$inputLibraryFile,
			 "R=s" => \$inputReadFile,
			 "T=s" => \$threshold,
			 'strain!' => \$dostrain,
			 "help|h|man" => \$help,
			 'usearch!' => \$usearchflag,
			 'rdp!' => \$rdp_option);

if ( (! $inputLibraryFile) || (! $inputReadFile) || (! $threshold) ){
    print_help();
    die "Usage: perl $0 -l reference_library_file -r read_file [-t identity-threshold] [-rdp] [-strain] [-nousearch]\n";
}
if ($help) {print_help(); exit;}

print "STIRRUPS - $version\nRun Settings:\n\n";
print "Reference File\t$inputLibraryFile\n",
    "Read File\t$inputReadFile\n",
    "Threshold\t$threshold\n";

if ($usearchflag) {print "Run USEARCH\tyes\n"} else {print "Run USEARCH\tno\n"};
if ($dostrain) {print "Strain Analysis\tyes\n"} else {print "Strain Analysis\tno\n"};
if ($rdp_option) {print "RDP Analysis\tyes\n"} else {print "RDP Analysis\tno\n\n"};

print STDERR "Building hashes for taxa ids - please wait...\n";

#------------------------------------------------------------------------------
#                          GetTaxIds
#------------------------------------------------------------------------------

my %taxidHash;
my @taxIDs;
my $taxidLine;

my $taxidFile = File::Spec->catdir($BASEDIR, "taxidfile.txt");
open TF, "$taxidFile" or die "Error in opening $taxidFile\n";

while($taxidLine = <TF>){
    $taxidLine =~ s/\t//g;
    @taxIDs = split(/\|/,$taxidLine);
    if(!defined $taxidHash{$taxIDs[1]}){
	$taxidHash{$taxIDs[1]} = $taxIDs[0];
    }
}

#------------------------------------------------------------------------------
#                          GetTaxon Heirarchy
#------------------------------------------------------------------------------

my %taxHierarchy;
my @taxH;
my $taxHline="";
my $genustag="";
my $taxonH="";
my $taxHfile = File::Spec->catdir($BASEDIR, "taxon_hierarchy.txt");

open TH, "$taxHfile" or die "Error in opening $taxHfile\n";

while($taxHline = <TH>){
    $taxHline =~ s/\s+$//; #OS independent chomp 
    @taxH = split("\t",$taxHline);
    while(length(@taxH) >=2){
	
	$genustag = shift(@taxH);
	$taxonH = join(';',@taxH);
	if(!defined $taxHierarchy{$genustag}){
	    $taxHierarchy{$genustag} =  $taxonH;
	}
	shift(@taxH);
    }
}

#------------------------------------------------------------------------------
#     				Run Uclust
#------------------------------------------------------------------------------

my ($genusName,$command,$method);
($genusName) = $inputReadFile =~ /(.*)?\..*/;

my $usearch = "./usearch5";
$usResultsFile = "${genusName}_usearch_results_${threshold}.txt";
print "Reading $inputLibraryFile ...\n";
if($usearchflag) {	
    $command = "$usearch --query $inputReadFile --db $inputLibraryFile --uc $usResultsFile --id 0.0 --maxaccepts 9 --maxrejects 0 --nowordcountreject --global";
    print "Running USEARCH $command ...\n";
    system($command);
}
$method = 0;
calculateSampleComposition($usResultsFile,$genusName,$threshold, $method);  #call the subroutine to parse and run stats on usearch results
	       
#------------------------------------------------------------------------------
#     			Composition Subroutine
#------------------------------------------------------------------------------

sub calculateSampleComposition{
    #------------------------File Handle-----------------------------------
    print "Parsing USEARCH output... \n";
    my $file = $_[0];		#Name of usearch output file
    my $genus = $_[1];		#Name of current genus
    my $cutoff = $_[2];		#filter by user input threshold
    my $method_id = $_[3];
    if(!defined($file)){
	die "USAGE: perl $0 Cannot Find Usearch output\n";
    }
    open FH, "$file" or die "Error in opening $file\n";
    
    my $assignmentOutputFile = "$genus"."_assignment_$cutoff.txt";  #create and open output filse
    my $summaryOutputFile = "$genus"."_summary_$cutoff.txt";
    my $summaryOutputFile2 = "$genus"."_summary_rdp_$cutoff.txt";
    my $summaryOutputFile3 = "$genus"."_summary_taxonomy_$cutoff.txt";
    my $strainfile = "$genus"."_summary_strain_$cutoff..txt";
    open WA, ">$assignmentOutputFile";
    open WS, ">$summaryOutputFile";
    
    if ($rdp_option) { 
	open WS2, ">$summaryOutputFile2";
	open WS3, ">$summaryOutputFile3";
    }

    if ($dostrain) {open WM, ">$strainfile";}
    #-----------------------Variables-------------------------------------
    
    my ($line,$query,$target,$score,$level,$rank,$name,$strain,$species,$tax_id);
    my ($tempscore,$tempsamp,$temptaxa,$temptaxastrain,$temprdpterminal,$temprdpconfidence,$temprdplevel);
    my $sample_id="";
    my $read_id="";
    my $taxaGS="";
    my $taxaSS="";
    my $count=0;
    my $gi_id="";
    my $maxScore = 0;
    my $prev_id="";
    my $percentage=0;					#calculate percentage of species and/or strain content of each sample id based on
    my $percentageStrain=0;				#counts in the appropriate hashes and output them to the summary file
    
    my ($rdp_terminal,$rdp_level,$rdp_confidence);	
    my %readHash;
    my %speciesHash;
    my %strainHash;
    my %sampleSpeciesCountHash;
    my %sampleStrainCountHash;
    my %species_rdpterminal_Hash;
    
    my @lineArray;
    my @queryArray;
    my @referenceArray;
    my @speciesArray;
    my @strainArray;
    my @readArray;
    
    #-------------------------Make Assignment File------------------------
    while($line = <FH>){
	
	if ($line !~ /^#/){
	    @lineArray = split('\t',$line);			#split by tabs, then put the info for the query and target into 
	    @queryArray = split(/\|/,$lineArray[8]);	    
	    $sample_id = $queryArray[0]; 
	    $read_id = $queryArray[1];   
	    if ($rdp_option) {
		$rdp_terminal = $queryArray[2];
		$rdp_level = $queryArray[3];
		$rdp_confidence = $queryArray[4];
		if($rdp_confidence =~ /^[a-zA-Z]+/) { 
		    $rdp_level = $queryArray[4];
		    $rdp_confidence = $queryArray[3];
		}
	    }

	    $level = 100;
	    $rank = "species";
	    
	    if($lineArray[0] eq 'H'){				#appropriate arrays, splitting by '|'.  Collect the read and sample IDs into variables
		$score = $lineArray[3];
		@referenceArray = split(/\|/,$lineArray[9]);
		$gi_id = $referenceArray[1];
		
		for (my $i=0; $i < scalar(@referenceArray); $i++){			#get the name of the genus and species 
		    if($referenceArray[$i] eq "genus"){
			$taxaGS = $referenceArray[$i+1]." ".$referenceArray[$i+3];
		    }
		}
		for (my $i=0; $i < scalar(@referenceArray); $i++){			#same as species level only strain name added
		    if($referenceArray[$i] eq "strain" ){
			if($referenceArray[$i+1] ne ''){
			    $taxaSS = $referenceArray[$i-3]." ".$referenceArray[$i-1]."|".$referenceArray[$i+1];
			} else { 
			    $taxaSS = $referenceArray[$i-3]." ".$referenceArray[$i-1]."|No Strain";
			}
			
		    }
		}
	    } else {  # All lines without Hit information ...
		$score = 0;
		$taxaGS = "No Hit";
		$taxaSS = "No Hit|No Strain";
	    }
	    
	    if( ($prev_id ne $read_id) || (eof(FH)) ){
		if(defined $readHash{$prev_id}){ #first time wont be defined
		    $tempscore = $readHash{$prev_id}[0];
		    $tempsamp = $readHash{$prev_id}[1];
		    $temptaxa = $readHash{$prev_id}[2];
		    $temptaxastrain = $readHash{$prev_id}[3];
		    $temprdpterminal = $readHash{$prev_id}[4];
		    $temprdpconfidence = $readHash{$prev_id}[5];
		    $temprdplevel = $readHash{$prev_id}[6];
		    
		    if($tempscore < $cutoff){
			$temptaxa = $temptaxa."|BT";
			if($temptaxastrain ne ""){
			    $temptaxastrain = $temptaxastrain."|BT";
			}
		    }else{
			$temptaxa = $temptaxa."|AT";
			if($temptaxastrain ne ""){
			    $temptaxastrain = $temptaxastrain."|AT";
			}
		    }
		    if(defined $speciesHash{$tempsamp} && defined $speciesHash{$tempsamp}{$temptaxa}){ #add species info for current sample_id
			$speciesHash{$tempsamp}{$temptaxa}[0] +=1;
			$speciesHash{$tempsamp}{$temptaxa}[1] += $tempscore;
		    }else{
			$speciesHash{$tempsamp}{$temptaxa}[0] = 1;			#keeps a count of number of times this genus/species combo exists for this sample id
			$speciesHash{$tempsamp}{$temptaxa}[1] += $tempscore;
		    }
		    if(defined $sampleSpeciesCountHash{$tempsamp}){		#keeps a count of the total number of records for htis sample id
			$sampleSpeciesCountHash{$tempsamp}++;
		    }else{
			$sampleSpeciesCountHash{$tempsamp} = 1;
		    }
		    
		    if(defined $taxidHash{$temptaxa}){
			$tax_id = $taxidHash{$temptaxa};
		    }elsif($temptaxa =~ /(.*)BT/){
			$name = $1;
			$name =~ s/\s+$//; 
			    if(defined $taxidHash{$name}){
				$tax_id = $taxidHash{$name};
			    }
		    }else{
			$tax_id = int(rand(10000) + rand(10000));
		    }
		    

		    if ($rdp_option) {print WA "$tempsamp|$prev_id|$temprdpterminal|$temprdpconfidence|$temprdplevel|$temptaxa|$tempscore|\n";}	
		    else {print WA "$tempsamp|$prev_id|$temptaxa|$tempscore|\n";}

		    #-----------------------------------------STRAIN LEVEL----------------------------------------------
		    if($dostrain){
			if($temptaxastrain ne ""){
			    $level = 101;
			    $rank = "strain";
			    
			    if(exists $strainHash{$tempsamp} && defined $strainHash{$tempsamp}{$temptaxastrain}){
				$strainHash{$tempsamp}{$temptaxastrain}[0] += 1;
				$strainHash{$tempsamp}{$temptaxastrain}[1] += $tempscore;
				
			    }else{
				$strainHash{$tempsamp}{$temptaxastrain}[0] = 1;	
				$strainHash{$tempsamp}{$temptaxastrain}[1] = $tempscore;
			    }
			    
			    if(exists $sampleStrainCountHash{$tempsamp}{$temptaxastrain}){
				$sampleStrainCountHash{$tempsamp}{$temptaxastrain}++;
			    }else{
				$sampleStrainCountHash{$tempsamp}{$temptaxastrain} = 1;
			    }
			    
			}
		    }
		    #--------------------------------------------------------------------------------------------
		    
		    if ($rdp_option) {
			if(defined $species_rdpterminal_Hash{$tempsamp}{$temptaxa}{$temprdpterminal}){ #add species info for current sample_id
			    $species_rdpterminal_Hash{$tempsamp}{$temptaxa}{$temprdpterminal}[0] +=1;
			    $species_rdpterminal_Hash{$tempsamp}{$temptaxa}{$temprdpterminal}[1] += $tempscore;
			    $species_rdpterminal_Hash{$tempsamp}{$temptaxa}{$temprdpterminal}[2] += $temprdpconfidence;
			}else{
			    $species_rdpterminal_Hash{$tempsamp}{$temptaxa}{$temprdpterminal}[0] = 1;      #count of number of times this genus/species combo exists for this sample id
			    $species_rdpterminal_Hash{$tempsamp}{$temptaxa}{$temprdpterminal}[1] += $tempscore;
			    $species_rdpterminal_Hash{$tempsamp}{$temptaxa}{$temprdpterminal}[2] += $temprdpconfidence;
			    $species_rdpterminal_Hash{$tempsamp}{$temptaxa}{$temprdpterminal}[3] = $temprdplevel;
			}
		    }

		    #-------------------------------------------------------------------------------------------
		}
	    }
	    
	    if(!defined $readHash{$read_id} || $score>$readHash{$read_id}[0]){
		$readHash{$read_id}[0] = $score;
		$readHash{$read_id}[1] = $sample_id;
		$readHash{$read_id}[2] = $taxaGS;
		$readHash{$read_id}[3] = $taxaSS;
		$readHash{$read_id}[4] = $rdp_terminal;
		$readHash{$read_id}[5] = $rdp_confidence;
		$readHash{$read_id}[6] = $rdp_level;
	    }
	    
	    $prev_id = $read_id;	
	}
    }
    
    
    #---------------------------Make Stat File------------------------------------------------------
    
    my $scoreavg=0;
    my $confidenceavg=0;
    my $maxperc = 0;
    my $maxperctaxa = "";
    my $scoreavgstrain =0;
    foreach my $sample ( keys %speciesHash ){
	foreach my $sample_taxa (sort {$speciesHash{$sample}{$b}[0] <=> $speciesHash{$sample}{$a}[0] }
				 keys %{$speciesHash{$sample}}){
	    
	    if(defined $speciesHash{$sample}{$sample_taxa} && $sampleSpeciesCountHash{$sample} !=0){
		$count = $speciesHash{$sample}{$sample_taxa}[0];
		$percentage = ($speciesHash{$sample}{$sample_taxa}[0]/$sampleSpeciesCountHash{$sample}) * 100;
		if($percentage > $maxperc){
		    $maxperc = $percentage;
		    $maxperctaxa = $sample_taxa;
		}
		$scoreavg = ($speciesHash{$sample}{$sample_taxa}[1]/$speciesHash{$sample}{$sample_taxa}[0]);
		
		if(defined $taxidHash{$sample_taxa}){
		    $tax_id = $taxidHash{$sample_taxa};
		}else{
		    $tax_id = int(rand(10000) + rand(10000));
		}
		print WS "$sample|$sample_taxa"
		    ."|$speciesHash{$sample}{$sample_taxa}[0]"
		    ."|$percentage|$scoreavg\n";
		
	    }
	    $maxperc = 0;
	    $maxperctaxa = "";
	    my $terminal="";
	    my $taxonomy = "";
	    my $tag = "";
	    my $ucgenus = "";
	    my $sum=0;


	    foreach my $rdp_terminal_taxa(keys %{$species_rdpterminal_Hash{$sample}{$sample_taxa}}){
		if(defined $species_rdpterminal_Hash{$sample}{$sample_taxa}{$rdp_terminal_taxa}){
		    $percentage = ($species_rdpterminal_Hash{$sample}{$sample_taxa}{$rdp_terminal_taxa}[0]/$speciesHash{$sample}{$sample_taxa}[0]) * 100;
		    $scoreavg = ($species_rdpterminal_Hash{$sample}{$sample_taxa}{$rdp_terminal_taxa}[1]/$species_rdpterminal_Hash{$sample}{$sample_taxa}{$rdp_terminal_taxa}[0]);
		    $confidenceavg = ($species_rdpterminal_Hash{$sample}{$sample_taxa}{$rdp_terminal_taxa}[2]/$species_rdpterminal_Hash{$sample}{$sample_taxa}{$rdp_terminal_taxa}[0]);
		    
		    if ($rdp_option) {
			print WS2 "$sample|$sample_taxa|$rdp_terminal_taxa|$confidenceavg"
			    ."|$species_rdpterminal_Hash{$sample}{$sample_taxa}{$rdp_terminal_taxa}[0]"
			    ."|$percentage|$scoreavg\n";
		    }
		    
		    if($sample_taxa =~ /BT/){
			$terminal = $rdp_terminal_taxa;
			$tag = "BT|RDP";			
			if(defined($taxHierarchy{$terminal})){
			    $taxonomy = $terminal.";".$taxHierarchy{$terminal};
			}
		    }else{
			$terminal = $sample_taxa;
			$tag = "UC";
			if($terminal =~ /(\w+)\s\w+\|AT/){
			    $ucgenus = $1;
			}
			
			if(defined($taxHierarchy{$ucgenus})){
			    $taxonomy = $ucgenus.";".$taxHierarchy{$ucgenus};
			}
			
		    }
		    
		    $sum+= $species_rdpterminal_Hash{$sample}{$sample_taxa}{$rdp_terminal_taxa}[0];		    
		}
	    }
	
	    if ($rdp_option) {
		print WS3 "$sample|$terminal|$tag|$taxonomy|$confidenceavg"
		    ."|$sum"
		    ."|$percentage|$scoreavg\n";
	    }
	}   
	if($dostrain){
	    if(exists $strainHash{$sample}){
		foreach my $sample_taxa_strain (sort {$strainHash{$sample}{$b}[0] <=> $strainHash{$sample}{$a}[0] } 
						keys %{$strainHash{$sample}}){
		    
		    
		    if(defined $strainHash{$sample}{$sample_taxa_strain} && $sampleSpeciesCountHash{$sample} != 0){
			$percentageStrain = ($strainHash{$sample}{$sample_taxa_strain}[0]/$sampleSpeciesCountHash{$sample}) * 100;
			$scoreavgstrain = ($strainHash{$sample}{$sample_taxa_strain}[1]/$strainHash{$sample}{$sample_taxa_strain}[0]);
			print WM "$sample|$sample_taxa_strain|$strainHash{$sample}{$sample_taxa_strain}[0]|$percentageStrain|$scoreavgstrain\n";
		    }
		}
	    }
	}
    }
    print STDERR "OUTPUT Assignment File = $assignmentOutputFile\n";
}#-----------------End of Sub---------------------------
    
#------------------------------------------------------------------------------
#                               Help Message
#------------------------------------------------------------------------------
    
    sub print_help{
	print STDERR <<EOF;
This program executes the stirrups software pipeline
	    
  Input:
    -L FILE     Reference Library File
    -R FILE     Input Reads File
    
  Options:
    -T VALUE    Identity Threshold [default = 97]
    -nousearch   Do not run USEARCH 
    -rdp        Fasta contains RDP data
    -strain     Perform Strain Level Analysis

EOF
}#end of help message
    
#------------------------------------------------------------------------------
#                               End of Program 
#------------------------------------------------------------------------------
