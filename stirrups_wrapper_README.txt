@###########################################################################@
USING STIRRUPS TO ADD SPECIES LEVEL ANNOTATION TO TAXONOMY FILE FROM MOTHUR
@###########################################################################@

example run:
perl stirrups_wrapper.pl -i example_input.fasta -s /usr/local/archdata/0534/projects/T1D-CNMC_archive/T1D_mothur_run_20150402/stability.contigs.good.groups -r /usr/local/archdata/0534/projects/T1D-CNMC_archive/T1D_mothur_run_20150402/stirrups_test/strep_ncbi_ref_sequences_annotated.fasta -x /usr/local/archdata/0534/projects/T1D-CNMC_archive/T1D_mothur_run_20150402/stirrups_test/stability.trim.contigs.good.unique.good.filter.unique.precluster.pick.pick.pds.wang.pick.taxonomy -g streptococcus


INPUT:
=====
- input fasta file (yap/mothur post chimera removal fasta file)
- groups file (from yap/mothur run; usually: stability.contigs.good.groups)
- pre-formatted reference file. see example file for format: /usr/local/archdata/0534/projects/T1D-CNMC_archive/T1D_mothur_run_20150402/stirrups_test/strep_ncbi_ref_sequences_annotated.fasta
- taxonomy file from mothur/yap (usually outputted by classify seqs; same number of reads as the input fasta file)
-  identity threshold [default = 97]
- genus name; eg: streptococcus. The sequences in the reference file must ideally belong to this genus


The stirrups results are merged back into the taxonomy file as follows:
- step through mothur taxonomy file from the classify seqs (same reads as the fasta file that was used for stirrups)
		- if read exists in stirrups output:
			- if in agreement with original taxonomy {print entire taxonomy as is and append species from stirrups output} {Else print CONFLICT FLAG}
		
		- else print original taxonomy


OUTPUT
======
example_input.formatted_usearch_results_97.txt [raw output from usearch]
example_input.formatted_assignment_97.txt [output from stirrups pipeline]
example_input.formatted_summary_97.txt [output from stirrups pipeline]
example_input.taxonomy [species information from stirrups merged into taxonomy file]


Note:
=====
A limitation of this wrapper is that it can only examine one genus per run. 
It requires a specialized reference sequence database for species belonging to that particular genus.

The stirrups_wrapper.pl script depends on the following:
./stirrups_pipeline_1.5.pl
./usearch5
./taxidfile.txt
./taxon_hierarchy.txt

		
===================================================
STIRRUPS DOCUMENTATION AND SOURCE CAN BE FOUND HERE:
http://sourceforge.net/projects/stirrups/files/?source=navbar






Fixes:
======
Spaces in species names are replaced with underscores (for Mothur compatibility)
Unclassified 7th level added to all lineages, including those not passed through Stirrups
