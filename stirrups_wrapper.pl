#!/usr/local/bin/perl
###############################################################################

use strict;
use FindBin ();
use Getopt::Std;
use FileHandle;
use vars qw($opt_i $opt_s $opt_r $opt_x $opt_t $opt_g);

getopts("i:s:r:x:t:g:");
my $usage = "usage: 
$0 
	-f <input fasta file (yap/mothur post chimera removal fasta file)>
	-s <groups file (from yap/mothur run; usually: stability.contigs.good.groups)>
	-r <preformatted reference file (see example file for format: /usr/local/archdata/0534/projects/T1D-CNMC_archive/T1D_mothur_run_20150402/stirrups_test/strep_ncbi_ref_sequences_annotated.fasta))>
	-x <taxonomy file from mothur/yap (usually outputted by classify seqs :same number of reads as the input fasta file)>
	-t <(identity threshold [default = 97])>
	-g <genus name; eg: streptococcus. The sequences in the reference file must ideally belong to this genus>
	
eg:
perl $0 -i stability.trim.contigs.good.unique.good.filter.unique.precluster.pick.pick.pick.fasta -s stability.contigs.good.groups -r /usr/local/archdata/0534/projects/T1D-CNMC_archive/T1D_mothur_run_20150402/stirrups_test/strep_ncbi_ref_sequences_annotated.fasta -t stability.trim.contigs.good.unique.good.filter.unique.precluster.pick.pick.pds.wang.pick.taxonomy -g streptococcus
";

# EXTERNAL SOFTWARE AND DEPENDENCIES:
# This script depends on the following:
my $STIRRUPS = "$FindBin::Bin/stirrups_pipeline_1.5.pl";
my $USEARCH = "$FindBin::Bin/usearch5";
my $TAXID = "$FindBin::Bin/taxidfile.txt";
my $TAXHIERARCHY = "$FindBin::Bin/taxon_hierarchy.txt";

# VALIDATE INPUT:
if(!(
	defined($opt_i) && 
	defined($opt_s) && 
	defined($opt_r) && 
	defined($opt_x) && 
	defined($opt_g))){
	die $usage;
}
my $input=$opt_i;
my $groups=$opt_s;
my $ref=$opt_r;
my $genus=$opt_g;
my $tax=$opt_x;
my $input_root;
my (%groups_hash, %stirrups_hash);

if ($input=~/(.*)\.fasta/){
	$input_root = $1;
}
else {
	print "input file does not seem to be a fasta file.\n"; die;
}

	
# READING GROUPS FILE INTO HASH:
open GROUPS, $groups or die "Cannot open $groups for read :$!";
while (<GROUPS>) {
	chomp;
	my @groups=split /\t/, $_;
	$groups_hash{$groups[0]}=$groups[1];
}
close (GROUPS);


# REFORMATTING INPUT FILE DEFLINES TO SUIT STIRRUPS:
my $formatted_input = $input_root.".formatted.fasta";
open IN, $input or die "Cannot open $input for read :$!";
open (OUT, ">$formatted_input") or die "Cannot open $formatted_input to write :$!";;

while (<IN>) {
	chomp;
	my $line=$_;
	if ($line=~/^>(.*)/){
		if (exists $groups_hash{$1}) {
			print OUT ">$groups_hash{$1}|$1\n";
		}
		else{
			print OUT ">NOSAMPLE|$1\n";
		}
	}
	else {
		print OUT "$line\n";
	}
}
close (OUT);
close (IN);


# RUN STIRRUPS:
my $stirrups_str = $STIRRUPS." -l ".$ref." -r ".$formatted_input;
my $stirrups_output = $input_root.".formatted_assignment_97.txt";

if (defined($opt_t)){
	$stirrups_str = $stirrups_str . " -t ". $opt_t;
	$stirrups_output = $input_root."_assignment_".$opt_t.".txt";
}
print "\n# RUNNING STIRRUPS\nexecuting $stirrups_str\n";
print "\n$stirrups_output\n";
system($stirrups_str);


# READ (ABOVE THRESHOLD) READS INTO A HASH:
open S_IN, $stirrups_output or die "Cannot open $stirrups_output for read :$!";
while (<S_IN>) {
	chomp; 
	my $stirrups_out=$_;
	my @stirrups_out=split /\|/, $stirrups_out;
	if ($stirrups_out[3] eq "AT"){
		$stirrups_hash{$stirrups_out[1]}=$stirrups_out[2];
	}
}
close (S_IN);


# REWRITE TAXONOMY FILE:
my $new_tax = $input_root.".taxonomy";
open T_IN, $tax or die "Cannot open $tax for read :$!";
open (T_OUT, ">$new_tax") or die "Cannot open $new_tax to write :$!";;
while (<T_IN>) {
	chomp; 
	my $tax_line=$_;
	my ($read,$taxonomy)=split /\t/, $tax_line;
	if (exists $stirrups_hash{$read}) {
		if ($taxonomy=~/$genus/i) {
			$stirrups_hash{$read} =~ s/\s/_/;
			$taxonomy = $taxonomy.$stirrups_hash{$read}."(100);";
			print T_OUT "$read\t$taxonomy\n";
		}
		else{
			print T_OUT "$tax_line\tCONFLICT: STIRRUPS found $stirrups_hash{$read}\n";
		}
	}
	else{
		print T_OUT "$tax_line"."unclassified;\n";
	}
}
close(T_OUT);
close (T_IN);
